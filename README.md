<snippet>
  <content>
## ${1:Project Generador}

TODO: genera 3 scrip para poblar base de datos
	1. factura.sql
	2. producto.sql
	3. detalle.sql

## Software 
	-PostgreSQL
	-JAVA 8

## Instructions
	1- Crear una DATABASE
	2- ejecutar dentro de esa DATABASE el scrip `base.sql`
	3- Entrar a la raiz  de el proyecto /base   
	4- En la terminal compiar los archivos  `javac Factura.java Producto.java Detalle.java Tesis.java`
	5- Ejecutar el main que se  Tesis.java  `java Tesis 0<nuemroDePersonas 0<nComprasAleatoriasMAXIMAS 0<restarA2017<2016`
	* 30=nuemroDePersonas 	10=nComprasAleatoriasMAXIMAS 	restarA2017= 2017-3 "empezara de 2014 a 2017"
	* ejejmplo : `java Tesis 30 10 3`

	6- ejecutar los archivos que genrero en la DATABASE en el sigueinte orden 
		- `producto.sql`
		- `factura.sql`
		- `detalle.sql`
## Credits
	Autor: Rodrigo Rivera Paz
## License
	Licencias GPL
></content>
  
</snippet>