

create table Factura (
  idFactura int,
  idCliente int,
  fecha date,
  categoriaCliente int,
  PRIMARY KEY (idFactura)
 );


create table Producto (
  idProducto int,
  nombre varchar(40),
  departamento varchar(40),
  precio real,
  PRIMARY KEY (idProducto)
 );

create table Detalle (
  iddetalle int,
  idfactura int,
  idProducto int ,
  cantidad int ,
  precio real,
  PRIMARY KEY (iddetalle),
  FOREIGN KEY (idfactura)REFERENCES Factura(idfactura),
  FOREIGN KEY (idProducto)REFERENCES Producto(idProducto)
 );