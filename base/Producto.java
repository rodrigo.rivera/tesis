

/**
 *
 * @author rodrigorivera
 */
public class Producto {

    private String id;
    private String departamento;
    private String nombre;
    private String precio;

    public Producto() {
    }

    public Producto(String id, String departamento, String nombre, String precio) {
        this.id = id;
        this.departamento = departamento;
        this.nombre = nombre;
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String sql() {
        
        return "INSERT INTO Producto (idProducto,nombre,departamento,precio) VALUES "
                + "(" + id + ", '" + nombre + "' , '" + departamento + "' ," + precio + ");\n";
    }

}
