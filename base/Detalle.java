
/**
 *
 * @author rodrigorivera
 */
public class Detalle {

    private String iDdetalle;
    private String iDfactura;
    private String iDproducto;
    private String cantidad;
    private String precio;

    public Detalle() {
    }

    public Detalle(String iDdetalle, String iDfactura, String iDproducto, String cantidad, String precio) {
        this.iDdetalle = iDdetalle;
        this.iDfactura = iDfactura;
        this.iDproducto = iDproducto;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public String getiDdetalle() {
        return iDdetalle;
    }

    public void setiDdetalle(String iDdetalle) {
        this.iDdetalle = iDdetalle;
    }

    public String getiDfactura() {
        return iDfactura;
    }

    public void setiDfactura(String iDfactura) {
        this.iDfactura = iDfactura;
    }

    public String getiDproducto() {
        return iDproducto;
    }

    public void setiDproducto(String iDproducto) {
        this.iDproducto = iDproducto;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "idDetalle :" + iDdetalle + " idFactura:" + iDfactura + " idProducto:" + iDproducto + " cantidad:" + cantidad + " precio:" + precio;
    }

    public String sql() {
        return "INSERT INTO Detalle (iddetalle,idfactura,idProducto,cantidad,precio)VALUES("+
                iDdetalle+","+iDfactura+","+iDproducto+","+cantidad+","+precio+");\n";
    }

}
