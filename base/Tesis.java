

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author rodrigorivera
 */
public class Tesis {

    /**
     * @param 
     * args[0] = nuemro de personas 
     * args[1] = nCompras cunatas compras realiza una perona de 1 a nCompras
     * args[2] = año años que le vamos a restar a 2017
     */
    public static void main(String[] args) {
        // TODO code application logic here

        Tesis t = new Tesis();

        ArrayList<Factura> facturas = t.facutra(Integer.valueOf(args[0]), Integer.valueOf(args[1]), Integer.valueOf(args[2]));
        t.escribirFacturas(facturas);

        ArrayList<Producto> prductos = t.productos("baseProductos/productos.csv", facturas.size());
        t.escribirProductos(prductos);

        ArrayList<Detalle> detalle = t.detalle(facturas, prductos);
        t.escribirDetalle(detalle);
    }

    /**
     * crear una elementos aleatorios para la tabla factura
     *
     * @param personas numero que depersonas diferentes > 0
     * @param nCompras cunatas compras realiza una perona de 1 a nCompras
     * @param año años que le vamos a restar a 2017
     */
    private ArrayList<Factura> facutra(int personas, int nCompras, int año) {

        int id = 1;
        Factura factura;
        ArrayList<Factura> lista = new ArrayList();
        for (int pers = 1; pers <= personas; pers++) {
            int numAleatorioCompras = (int) (Math.random() * nCompras) + 1;
            int categoria = (int) (Math.random() * 3) + 1;
            for (int j = 0; j < numAleatorioCompras; j++) {
                String fechaA = fecha(año);
                factura = new Factura();
                factura.setId(id + "");
                factura.setFecha(fechaA);
                factura.setIdCliente(pers + "");
                factura.setCategoriaCliente(categoria + "");
                lista.add(factura);
                id++;
            }
        }
        return lista;
    }

    /**
     *
     * @param año
     * @return
     */
    private String fecha(int año) {

        int anoMe = (int) (Math.random() * (año + 1)) + 0;
        int ano = 2017 - anoMe;
        int mes = (int) (Math.random() * 12) + 1;
        int dia = (int) (Math.random() * 30) + 1;
        while (mes == 2 && dia > 28) { // elimna 28 y 29  de febreo 

            dia = (int) (Math.random() * 31) + 1;
        }
        String fecha = "'" + ano + "-" + mes + "-" + dia + "'";
        return fecha;
    }

    /**
     * el archivo que vamos a leer "productos.csv" tiene 88476 y solo vamos a
     * escojer nArticulos los siguientes columnas: Retailer country,Order method
     * type,Retailer,type,Product line,Product type,
     * Product,Year,Quarter,Revenue,Quantity,Gross margin solo vamos a necesitar
     * el , product,Porduct type ,precio deje los otros por si se llegaran a
     * ocupar
     *
     * @param nombre_archivo
     * @param nArticulos numero de articulos de los articulos<=88476 @ return
     * una muestra de los nArticulos
     */
    private ArrayList<Producto> productos(String nombre_archivo, int nArticulos) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        Producto producto;

        ArrayList<Producto> temp = new ArrayList();
        ArrayList<Producto> productos = new ArrayList();
        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            archivo = new File(nombre_archivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            int i = 0;
            while ((linea = br.readLine()) != null) {
                producto = new Producto();
                i++;
                String[] divicion = linea.split(",");
                String departamento = divicion[4];
                String nombre = divicion[5];
                String dinero = divicion[8];

                producto.setId(i + "");
                producto.setNombre(nombre);
                producto.setDepartamento(departamento);
                producto.setPrecio(dinero);
                temp.add(producto);

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        //System.out.println("" + temp.size());
        for (int i = 0; i < nArticulos; i++) {
            int ramdom = (int) (Math.random() * temp.size()) + 1;
            productos.add(temp.get(ramdom));
        }

        return productos;
    }

    /**
     * crear la los elementos de la tabla Detalle,
     *
     * @param fac la lisata de todas la facturas
     * @param prod los poductos seleccionados de la base de datos
     * @return elementos de de la tabla
     */
    public ArrayList<Detalle> detalle(ArrayList fac, ArrayList prod) {

        ArrayList detalle = new ArrayList();
        ArrayList factura = fac;
        ArrayList productos = prod;
        Detalle temp_detalle;
        Factura f;
        Producto p;
        int numeroAleatorio;
        for (int i = 0; i < factura.size(); i++) {

            int canti = (int) (Math.random() * 2) + 1;
            p = (Producto) productos.get(i);
            f = (Factura) factura.get(i);
            temp_detalle = new Detalle();
            temp_detalle.setiDdetalle(i + "");
            temp_detalle.setiDfactura(f.getId());
            temp_detalle.setiDproducto(p.getId());
            temp_detalle.setCantidad(canti + "");
            Double a = Double.valueOf(p.getPrecio()) * canti;
            temp_detalle.setPrecio(a + "");
            detalle.add(temp_detalle);
        }

        return detalle;

    }

    /**
     * escribe un archivo "sql" en la raiz de la tabla Factura
     *
     * @param facturas
     */
    private void escribirFacturas(ArrayList<Factura> facturas) {
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter("factura.sql");
            pw = new PrintWriter(fichero);

            for (Factura factura : facturas) {
                pw.print(factura.sql());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /**
     * escribe un archivo "sql" en la raiz de la tabla Producto
     *
     * @param prductos
     */
    private void escribirProductos(ArrayList<Producto> prductos) {
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter("producto.sql");
            pw = new PrintWriter(fichero);

            for (Producto prducto : prductos) {
                pw.print(prducto.sql());

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    /**
     * escribe un archivo "sql" en la raiz de la tabla detalle
     *
     * @param detalle
     */
    private void escribirDetalle(ArrayList<Detalle> detalle) {

        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter("detalle.sql");
            pw = new PrintWriter(fichero);

            for (Detalle d : detalle) {
                pw.print(d.sql());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero) {
                    fichero.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

    }

}
