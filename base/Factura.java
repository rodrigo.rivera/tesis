

/**
 *
 * @author rodrigorivera
 */
public class Factura {

    private String idFactura;
    private String fecha;
    private String idCliente;
    private String categoriaCliente;

    public Factura() {
    }

    public Factura(String id, String fecha, String idCliente, String categoriaCliente) {
        this.idFactura = id;
        this.fecha = fecha;
        this.idCliente = idCliente;
        this.categoriaCliente = categoriaCliente;
    }

    public String getId() {
        return idFactura;
    }

    public void setId(String id) {
        this.idFactura = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getCategoriaCliente() {
        return categoriaCliente;
    }

    public void setCategoriaCliente(String categoriaCliente) {
        this.categoriaCliente = categoriaCliente;
    }

    @Override
    public String toString() {
        return "idfactura: " + idFactura + " fecha:" + fecha + " idCleinte:" + idCliente + " categoria:" + categoriaCliente;
    }

    public String sql() {
        return "INSERT INTO Factura(idFactura,idCliente,fecha,categoriaCliente)VALUES(" + idFactura
                + "," + idCliente + "," + fecha + "," + categoriaCliente + ");\n";

    }

}
